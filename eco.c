#include <stdlib.h>
#include <errno.h>
#include <gtk/gtk.h>
#include <glib/gprintf.h>

#if !GTK_CHECK_VERSION(3, 0, 0)
#include <gdk/gdk.h>
#include <glib.h>
#endif

static void submit(GtkWidget *w, gpointer e)
{
    g_printf("%s\n", gtk_entry_get_text(GTK_ENTRY(e)));
    gtk_main_quit();
}

int main(int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *vbox;
    GtkWidget *label;
    GtkWidget *entry;
    GtkWidget *halign;
    GtkWidget *hbox;
    GtkWidget *button_ok;
    GtkWidget *button_cancel;
    gchar *title = NULL;
    gchar *label_str = NULL;

    gtk_init(&argc, &argv);

    while(argc > 0)
    {
        if(!(g_strcmp0(*argv, "-title")) && (argc > 1))
        {
            argc--;
            argv++;
            title = g_strdup(*argv);
            continue;
        }

        if(!(g_strcmp0(*argv, "-label")) && (argc > 1))
        {
            argc--;
            argv++;
            label_str = g_strdup(*argv);
            continue;
        }

        if(g_str_has_prefix(*argv, "[sudo] password for "))
        {
            title = g_strdup("Password");
            label_str = g_strdup_printf("\
The application needs administrative privileges. \
Please enter password for %s", *argv+20);
            break;
        }

        argc--;
        argv++;
    }

    if(!title) title = g_strdup("Text Entry");
    if(!label_str) label_str = g_strdup("\
Text entered here will be printed to stdout, when you press \"Ok\".");

    /* window */
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), title);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER (window), 10);

    /* vbox : label, entry, buttons hbox */
#if GTK_CHECK_VERSION(3, 2, 0)
    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    gtk_box_set_homogeneous(GTK_BOX(vbox), FALSE);
#else
    vbox = gtk_vbox_new(FALSE, 3);
#endif
    gtk_container_add(GTK_CONTAINER(window), vbox);

    /* label */
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), label_str);
    gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
    gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);

    /* entry */
    entry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), entry, TRUE, TRUE, 0);

    /* right alignemnt */
    halign = gtk_alignment_new(1, 0, 0, 0);
    gtk_box_pack_start(GTK_BOX(vbox), halign, FALSE, FALSE, 0);

    /* hbox : ok button, cancel button */
#if GTK_CHECK_VERSION(3, 2, 0)
    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
    gtk_box_set_homogeneous(GTK_BOX(hbox), TRUE);
#else
    hbox = gtk_hbox_new(TRUE, 1);
#endif
    gtk_container_add(GTK_CONTAINER(halign), hbox);

    /* ok button */
    button_ok = gtk_button_new_from_stock(GTK_STOCK_OK);
    gtk_container_add(GTK_CONTAINER(hbox), button_ok);

    /* cancel button */
    button_cancel = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
    gtk_container_add(GTK_CONTAINER(hbox), button_cancel);

    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(window, "delete-event", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(entry, "activate", G_CALLBACK(submit), (gpointer)entry);
    g_signal_connect(button_ok, "clicked", G_CALLBACK(submit), (gpointer)entry);
    g_signal_connect(button_cancel, "clicked", G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show_all(window);

    gtk_main();

    g_free(title);
    g_free(label_str);

    return 0;
}
