PATH_PREFIX = /usr/local
PATH_BIN = $(PATH_PREFIX)/bin

INSTALL = /usr/bin/install
INSTALL_BIN = /usr/bin/install -m 755

# program name
bin = eco
objects =

CC = gcc
CFLAGS = -O3 -Wall
LDFLAGS =

# gtk+ 2.10 or newer needed for tray icon
GTK_VERSION = gtk+-2.0
#GTK_VERSION = gtk+-3.0

# debug
#CFLAGS += -D_DEBUG

CFLAGS += `pkg-config --cflags $(GTK_VERSION)`
LDFLAGS += `pkg-config --libs $(GTK_VERSION)`

## all: compile binary
all: $(bin)

$(bin): eco.c $(objects)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

help: commands

## commands: show accepted commands
commands:
	grep -E '^##' Makefile | sed -e 's/## //g'

## install: install to dir set in Makefile
install: $(bin)
	$(INSTALL) -d $(PATH_BIN)/
	$(INSTALL_BIN) -T $(bin) $(PATH_BIN)/$(bin)

## uninstall: remove installed files
uninstall:
	rm -r -f $(PATH_BIN)/$(bin)

## clean: remove compiled files (not the installed ones)
clean: 
	rm $(bin) $(objects)
