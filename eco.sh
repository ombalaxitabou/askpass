#!/bin/sh

if [ -z $SUDO_ASKPASS ]
then
    export SUDO_ASKPASS="/usr/local/bin/eco"
fi

sudo -A "$@"
